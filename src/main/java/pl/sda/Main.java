package pl.sda;

import java.util.Arrays;
import java.util.stream.IntStream;

public class Main {

    public static void main(String[] args) {
        int arrayToSort[] = {1,2,2,2,2,0,3,5,2};
        int searchingFor = 9;
        int[] sortedArray = countingSort(arrayToSort, 5);
        //bubbleSort(arrayToSort);
        System.out.println(Arrays.toString(sortedArray));
        /*int foundIndex = binarySearch(arrayToSort, searchingFor);
        System.out.println(foundIndex);*/
    }

    private static int[] countingSort(int[] arrayToSort, int maxElement) {
        int[] arrayWithCounts = new int[maxElement + 1];
        int[] sortedArray = new int[arrayToSort.length];
        for(int i = 0; i < arrayToSort.length; i++) {
            int value = arrayToSort[i];
            arrayWithCounts[value] ++;
        }
        int currentInsertIndex = 0;
        for(int i = 0; i < arrayWithCounts.length; i++) {
            int amountOfIndex = arrayWithCounts[i];
            for(int j = 0; j < amountOfIndex;j ++) {
                sortedArray[currentInsertIndex] = i;
                currentInsertIndex ++;
            }
        }
        return sortedArray;
    }

    private static int binarySearch(int[] arrayToSearch, int searchingFor) {
        int l = 0;
        int p = arrayToSearch.length - 1;
        int middle = (l + p) / 2;
        while (l < p) {
            if (arrayToSearch[middle] < searchingFor) {
                l = middle + 1;
            } else {
                p = middle;
            }
            middle = (l + p) / 2;
        }
        if (arrayToSearch[middle] == searchingFor) {
            return middle;
        } else {
            return -1;
        }
    }

    private static void printStar(int i) {
        IntStream.range(0, i)
                .forEach(e -> System.out.print("*"));
        System.out.println();
    }

    private static void printArrayWithStarts(int[] arrayToPrint) {
        for (int element : arrayToPrint) {
            printStar(element);
        }
        System.out.println("----------------");
    }

    private static void bubbleSort(int[] arrayToSort) {
        int n = arrayToSort.length;
        do {
            for (int i = 0; i < n - 1; i++) {
                //System.out.println(Arrays.toString(arrayToSort));
                if (arrayToSort[i] > arrayToSort[i + 1]) {
                    int tmp = arrayToSort[i + 1];
                    arrayToSort[i + 1] = arrayToSort[i];
                    arrayToSort[i] = tmp;
                }
                printArrayWithStarts(arrayToSort);
            }
            n--;
        } while (n > 1);
    }
}
